import { Tweet } from "../data";

export type TweetProps = { item: Tweet };
