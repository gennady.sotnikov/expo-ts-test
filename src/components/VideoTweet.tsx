import { Video } from 'expo-av';
import { FC } from 'react';
import { TweetProps } from './core';

export const VideoTweet: FC<TweetProps> = ({ item }) => {
    const hardcoddedVideoLink = 'https://www.videvo.net/video/abstract-rotating-cubes/1651/';
    const hardcoddedPreview = 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg';
    return <Video useNativeControls source={{ uri: hardcoddedVideoLink }} posterSource={{ uri: hardcoddedPreview }} />
}