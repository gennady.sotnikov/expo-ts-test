import { FC, useState } from "react";
import { WebView } from "react-native-webview";

const viewPort = '<meta name="viewport" content="width=device-width, initial-scale=1.0">';

type WebTextProp = { text: string };

export const WebText: FC<WebTextProp> = ({ text }) => {
    const [webHeight, setWebHeight] = useState(50);
    return text 
        ? <WebView
                style={{ height: webHeight }}
                scrollEnabled={false}
                source={{html: viewPort + text}}
                automaticallyAdjustContentInsets={false}
                onMessage={(event) => setWebHeight(Number(event.nativeEvent.data))}
                injectedJavaScript='window.ReactNativeWebView.postMessage(document.body.scrollHeight)'
            />
        : null;
}
