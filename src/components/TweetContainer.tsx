import moment from "moment";
import { FC } from "react";
import { View, StyleSheet, Text } from "react-native";
import { TweetProps } from "./core";
import { WebText } from "./WebText";

type TweetContainerProps = TweetProps & { children: React.ReactNode };

export const TweetContainer: FC<TweetContainerProps> = ({ children, item }) => {
    return (
        <View style={styles.tweetContainer}>
            <Text style={styles.date}>{moment(item.Created).format('MMM YY')}</Text>
            <Text>Type: { item.Type }</Text>
            <WebText text={item.Comment}></WebText>
            {children}
        </View>
    );
};

const styles = StyleSheet.create({ 
    tweetContainer: { borderColor: '#1DA1F2', borderWidth: 1, borderRadius: 30, padding: 20, marginBottom: 10 },
    date: { textAlign: 'right', color: 'rgb(83, 100, 113)' }
});