import React from "react";
import { FC } from "react"
import { Image, StyleSheet, View } from "react-native";
import { usePhoto } from "../data"
import { TweetProps } from "./core"

export const PhotoTweet: FC<TweetProps> = ({ item }) => {
    const photo = usePhoto(item.Id);
    const hardcoddedPreview = 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg';
    return (
        <View style={styles.container}>
            <Image style={styles.img} source={{ uri: hardcoddedPreview }} />
        </View>
    );
}

const styles = StyleSheet.create({ img: { width: 50, height: 50 }, container: { flex: 1 }});