const apiUrlBase = "http://65.21.61.26:5555/api/tweets";

export type ServerResponse<T> = {
  message: string;
  data: T;
};

export const appFetch = <T>(path: string) =>
  fetch(apiUrlBase + path).then(
    (res) => res.json() as unknown as ServerResponse<T>
  );
