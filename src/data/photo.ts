import { useEffect, useState } from "react";
import { appFetch } from "./core";

export type Photo = { id: string; url: string };

export const usePhoto = (tweetId: string) => {
  const [photo, setPhoto] = useState<Photo>();

  const initPhoto = async () => {
    const { data } = await appFetch<Photo>(`/${tweetId}/photos`);
    setPhoto(data);
  };

  useEffect(() => {
    initPhoto();
  }, []);

  return photo;
};
