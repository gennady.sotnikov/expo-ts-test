import { useEffect, useState } from "react";
import { appFetch } from "./core";

export enum TweetTypeEnum {
  Text = 1,
  Photo,
  Video,
  Timelog,
}

export type Tweet = {
  Id: string;
  Created: Date;
  Type: TweetTypeEnum;
  VideoUrl: string;
  VideoThumbnail: string;
  Comment: string;
};

export const useTweets = (page: number) => {
  const [tweets, setTweets] = useState<Tweet[]>([]);

  const updateTweets = async () => {
    const { data } = await appFetch<Tweet[]>(`?page=${page}`);
    setTweets([...tweets, ...data]);
  };

  useEffect(() => {
    updateTweets();
  }, [page]);

  return tweets;
};
