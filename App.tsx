import React from "react";
import { useState } from "react";
import { FlatList, StyleSheet, Text } from "react-native";
import { PhotoTweet, TweetContainer, VideoTweet } from "./src/components";
import { Tweet, TweetTypeEnum, useTweets } from "./src/data";

export default function App() {
  const [page, setPage] = useState(0);
  const tweets = useTweets(page);

  const getTweetComponent = (item: Tweet) => {    
    switch(item.Type) {
      case TweetTypeEnum.Photo: return <PhotoTweet item={item} />;
      case TweetTypeEnum.Video: return <VideoTweet item={item} />;
      default: null;
    }
  };

  return (
    <FlatList
      style={styles.container}
      data={tweets}
      onEndReached={() => setPage(page + 1)}
      renderItem={({ item }) => {
        return (
          <TweetContainer item={item}>
            {getTweetComponent(item)}
          </TweetContainer>
        );
      }}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    paddingHorizontal: 10
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
